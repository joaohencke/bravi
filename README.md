# Challange

# Instalation

### You can clone the repository `git clone git@gitlab.com:joaohencke/bravi.git ; cd bravi` 

### After clone, you must install dependencies running `npm install` at the root folder.

### I have choosen NodeJS for backend and React for the frontend. I know.. the job is for a Angular frontend developer but... the test was for fullstack developer and said ` "Bonus points for ES2015,Lint, Angular, React, TypeScript, Docker, Microservices"`.

### At the root folder, we have the frontend project at `webapp` and the backend at `server`

### This morning, I put my mom to 'play' and she found a bug and I have no time to fix it. You can test it by clicking at the columns/rows that indicates the coordinates.

### All requests to Chess Api will be saved to a MongoDB... Almost forgot that... you will need a `MongoDB` instance running too.

### The front end project was created using `create-react-app` but I made some modifications.. Added a `sass/stylus` pre-processor. (This not means that the Chessboard will look amazing)

### The favicon is the `Momo` (winged lemur from The Last Airbender). Why? I like him.

### You can run the project starting the backend and frontend with `npm start` on its folders (2 terminals);

### If you want to run a build version, you can build the project with `npm run build` and use `serve` package, to run a server.

# The project
### According to documentation, the api should return an array of positions... but I made a modification to return an object containing the first and the second turn... to display it on the board (where you can reach at the first and where you can reach at the second turn).
### And I forgot to put a button to submit the action. The action will be submitted to the server when you click at the coordinates.

### Thanks and sorry for some english mistakes :)