const ALPHABETH = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
const NUMERIC = ALPHABETH.map((letter, index) => index + 1);

module.exports = { ALPHABETH, NUMERIC };
