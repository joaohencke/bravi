const { superstruct } = require('superstruct');

exports.struct = superstruct({
  types: {
    empty: v => {
      if (v === '') return true;
      return false;
    },
  },
});

exports.formatErrorMessage = error => {
  let errMessage = `${error.message}`;
  if (error.reason) errMessage += ` - ${error.reason}`;
  if (errMessage.length > 250) {
    errMessage = `${errMessage.slice(0, 150)} ... ${errMessage.slice(errMessage.length - 100, errMessage.length)}`;
  }
  return errMessage;
};
