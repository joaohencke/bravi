const routes = require('./public/');

module.exports = app => {
  Object.keys(routes).forEach(path => {
    const endpoint = `/api/${path}`;
    const route = routes[path];

    if (route.$middleware) app.use(endpoint, route.$middleware);

    app.use(endpoint, route);
  });
};
