const express = require('express');
const mongoose = require('mongoose');
const config = require('./_config/config');

const app = express();

mongoose.connect(config.db);

const db = mongoose.connection;

db.on('error', () => {
  throw new Error(`Unable to connect to database at ${config.db}`);
});

require('./_config/express')(app);
require('./_config/routes')(app);

app.listen(8888, () => console.log('Express listening on port 8888'));
