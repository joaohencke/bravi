const express = require('express');
const mongoMorgan = require('mongo-morgan');
const ChessManager = require('./ChessManager');
const ChessValidation = require('./middleware/ChessValidation');
const config = require('./../_config/config');

const router = express.Router({ mergeParams: true });

module.exports = router;

router.$middleware = mongoMorgan(config.db, 'combined', { collection: 'logs' });

router.get('/:type(knight)/preview/:from', ChessValidation.preview(), (req, res, next) => {
  try {
    res.json(ChessManager.preview(req.validData));
  } catch (e) {
    next(e);
  }
});
