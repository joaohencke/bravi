const Piece = require('./model/Piece');

exports.preview = function preview({ type, from }) {
  const piece = new Piece(type);
  const firstTurn = [];
  const secondTurn = [];

  piece.preview({ from }).forEach(pos => {
    firstTurn.push(pos);
    secondTurn.push(...piece.preview({ from: pos, except: secondTurn }));
  });

  return { firstTurn, secondTurn };
};
