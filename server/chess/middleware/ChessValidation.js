const Boom = require('boom');
const { struct, formatErrorMessage } = require('../../util/validation/struct');

exports.preview = () => (req, res, next) => {
  try {
    const Schema = struct({
      type: 'string',
      from: 'string',
    });

    req.validData = {
      ...req.validData,
      ...Schema({
        ...req.params,
        type: req.params.type.toUpperCase(),
      }),
    };

    return next();
  } catch (e) {
    return next(Boom.badRequest(formatErrorMessage(err)));
  }
};
