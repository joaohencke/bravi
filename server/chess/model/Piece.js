const Move = require('./Move');
const { ALPHABETH, NUMERIC } = require('../../util');

class Piece {
  constructor(type) {
    if (!type) throw new Error('TYPE_NEEDED');
    if (!Piece.types[type]) throw new Error('NON_EXISTENT');

    this.type = type;
    this.moves = Piece.types[type].moves;
  }
  preview({ from, except = [] }) {
    const possibilities = [];

    const row = from.substring(0, 1);
    const col = Number(from.substring(1));
    const translatedRow = ALPHABETH.indexOf(row);

    this.moves.forEach(move => {
      const toRowForward = translatedRow + move.h;
      const toColForward = col + move.v;
      const toRowBackwards = translatedRow - move.h;
      const toColBackwards = col - move.v;

      const combinations = [
        {
          row: toRowForward,
          col: toColForward,
        },
        {
          row: toRowForward,
          col: toColBackwards,
        },
        {
          row: toRowBackwards,
          col: toColForward,
        },
        {
          row: toRowBackwards,
          col: toColBackwards,
        },
      ];

      combinations.forEach(to => {
        if (Move.validRange(to.row, to.col)) {
          const algebraicNotation = ALPHABETH[to.row] + to.col;
          if (
            Move.validCell(algebraicNotation) &&
            possibilities.indexOf(algebraicNotation) < 0 &&
            except.indexOf(algebraicNotation) < 0
          ) {
            possibilities.push(algebraicNotation);
          }
        }
      });
    });

    return possibilities;
  }
}

Piece.types = {
  KNIGHT: {
    moves: [new Move({ h: 1, v: 2 }), new Move({ h: 2, v: 1 })],
    key: 'KNIGHT',
  },
};

module.exports = Piece;
