const { ALPHABETH, NUMERIC } = require('../../util');

class Move {
  constructor({ h, v }) {
    this.h = h;
    this.v = v;

    this.totalRows = 8;
    this.totalCols = 8;
  }
  static validCell(cell = '') {
    const row = cell.substring(0, 1);
    const col = Number(cell.substring(1));
    return ALPHABETH.indexOf(row) >= 0 && NUMERIC.indexOf(col) >= 0;
  }
  static validRange(from, to) {
    return from >= 0 && from < 8 && to >= 0 && to < 8;
  }
}

module.exports = Move;
