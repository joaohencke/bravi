/* eslint global-require: 0 */
/* eslint import/no-dynamic-require: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-unused-vars: 0 */

const glob = require('glob');
const path = require('path');
const Boom = require('boom');
const express = require('express');

const rootPath = path.normalize(`${__dirname}/..`);

module.exports = app => {
  const apis = glob.sync(`${rootPath}/**/*Api.js`);
  apis.forEach(api => require(api)(app));

  app.use('/css', express.static(`${rootPath}/public/static/css`));
  app.use('/img', express.static(`${rootPath}/public/static/media`));
  app.use('/js', express.static(`${rootPath}/public/static/js`));
  app.use('/favicon', express.static(`${rootPath}/public/favicon.ico`));

  app.all('/*', (req, res) => {
    res.status(200).sendFile(`${rootPath}/public/index.html`);
  });

  app.use(express.static(`${rootPath}/public/static`));
  // Error handling
  app.use((err, req, res, next) => {
    const error = Boom.isBoom(err) ? err : Boom.boomify(err);
    if (process.env.NODE_ENV !== 'test') console.trace(error);
    return res.status(error.output.statusCode).json({
      error: error.name,
      message: error.message,
    });
  });
};
