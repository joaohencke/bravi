const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

module.exports = app => {
  app.use(morgan('dev'));
  app.use(
    bodyParser.json({
      limit: '1mb',
      type: 'application/json',
    }),
  );

  app.use(cors('*'));
};
