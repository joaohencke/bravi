import React from 'react';
import chessRoutes from './modules/chess/routes';
import { AppRouter } from './modules/common/components';

const routes = [].concat(chessRoutes || []);

const AppRoutes = () => (
  <div>
    {routes.map(((route, i) => <AppRouter key={i} {...route} />))}
  </div>
);

export default AppRoutes;
