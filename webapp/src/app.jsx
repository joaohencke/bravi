import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import registerServiceWorker from './registerServiceWorker';
import configStore from './_root/store';

import AppRoutes from './routes';

const store = configStore();

const Root = () => {
  return (
    <div className="container">
      <AppRoutes />
    </div>
  );
};

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Root></Root>
    </BrowserRouter>
  </Provider>
  , document.getElementById('root')
);

registerServiceWorker();
