import { combineReducers } from 'redux';
import chessReducer from '../modules/chess/reducers';

const root = combineReducers({
  chessReducer
});

export default root;
