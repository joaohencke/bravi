import axios from 'axios';

const ENDPOINT = 'http://localhost:8888/api/chess/';

class ChessApi {
  static preview({ piece, from }) {
    return axios.get(`${ENDPOINT + piece}/preview/${from}`)
      .then(res => {
        return {
          nextTurns: res.data,
          from: from,
        };
      });
  }
}

export default ChessApi;
