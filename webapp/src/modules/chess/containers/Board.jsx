import React from 'react';
import { ChessBoardRow } from '../components';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions/actions';

class Board extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.submit = this.submit.bind(this);

    this.state = {
      from: this.props.from,
      nextTurns: { firstTurn: this.props.nextTurns.firstTurn, secondTurn: this.props.nextTurns.secondTurn }
    };
  }
  submit(from) {
    const state = this.state;
    state.from = from;
    this.setState(state);
    this.props.preview({ from, piece: 'knight' });
  }
  render() {
    return (
      <div className="page-container chess-board">
        <div className="row">
          <div className="col">
            <header className="header">
              <h3>Chess board</h3>
            </header>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <ChessBoardRow size="8" submit={this.submit} from={this.state.from} />
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => state.chessReducer;
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Board);
