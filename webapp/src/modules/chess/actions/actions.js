import chessApi from '../api/chessApi';
import chessActions from './actionTypes';

export function preview(args) {
  return (dispatch) => chessApi.preview(args).then(res => dispatch(success(res)));
};

export function success(res) {
  return { type: chessActions.PREVIEW, ...res };
};
