import Board from './containers/Board';

const routes = [{
  path: '/',
  component: Board,
}];

export default routes;
