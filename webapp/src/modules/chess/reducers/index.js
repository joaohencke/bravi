import initialState from './initialState';
import actionTypes from '../actions/actionTypes';

export default function chessReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.PREVIEW:
      const from = action.from;
      const nextTurns = state.nextTurns;
      const { firstTurn, secondTurn } = nextTurns;

      firstTurn.length = 0;
      secondTurn.length = 0;

      firstTurn.push.apply(firstTurn, action.nextTurns.firstTurn);
      secondTurn.push.apply(secondTurn, action.nextTurns.secondTurn);
      return {
        ...state,
        from
      };
    default: {
      return {
        ...state
      };
    }
  }
}
