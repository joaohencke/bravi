import React from 'react';
import { PropTypes } from 'prop-types';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as chessActions from '../actions/actions';

class Square extends React.Component {

  static propTypes = {
    isBlack: PropTypes.bool.isRequired,
    value: PropTypes.string,
    nextTurns: PropTypes.object,
    submit: PropTypes.any,
    from: PropTypes.string,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      piece: 'knight',
      from: this.props.from,
      selected: false,
      value: this.props.value,
      nextTurns: this.props.nextTurns
    };
  }
  render() {
    const { isBlack } = this.props;
    const classes = classNames({
      'chess-square': true,
      'chess-square-black': isBlack,
      'chess-square-white': !isBlack,
      'selected': this.state.from === this.state.value
    }).concat(` ${this.props.className}`);

    return (
      <div className={classes} onClick={() => this.props.submit(this.state.value)}>
        {this.props.children}
        <p className="help-block">{this.state.nextTurns.firstTurn.map(cell => (cell === this.props.value ? '1o' : ''))}</p>
        <p className="help-block">{this.state.nextTurns.secondTurn.map(cell => (cell === this.props.value ? '2o' : ''))}</p>
      </div>
    )
  }
}

const mapStateToProps = state => ({ piece: 'knight', from: state.chessReducer.from, nextTurns: state.chessReducer.nextTurns });
const mapDispatchToProps = dispatch => bindActionCreators(chessActions, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(Square);
