import React from 'react';
import { ChessBoardColumn } from './';
const ChessBoardRow = ({ size = 8, submit, from }) => {
  const ALPHABETH = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

  const rows = [];

  rows.push(
    <div key={`row-header`} className="row chess-row chess-header" >
      <p className="text-monospace" >&nbsp;</p>
      <ChessBoardColumn row={90} header={true} />
    </div>
  );

  for (let i = 0; i < size; i++) {
    rows.push(
      <div key={`row-${i}`} className="row chess-row" >
        <p key={`row-${i}`} className="text-monospace" >{ALPHABETH[i]}</p>
        <ChessBoardColumn row={i} submit={submit} from={from} />
      </div>
    );
  }

  return rows;
};

export default ChessBoardRow;
