import React from 'react';
import { Square } from './';
import knight from '../../../assets/images/knight.png';

const ALPHABETH = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
const ChessBoardColumn = ({ row, submit, header = false, from }) => {
  const columns = [];
  const comparator = row % 2;

  ALPHABETH.map((letter, i) => {
    const algebraicNotation = `${ALPHABETH[row]}${i + 1}`;
    return columns.push(
      <Square submit={submit} key={`row-${row}-col-${i}`} isBlack={i % 2 === comparator && !header} className="col" value={algebraicNotation}>{header ? i + 1 : ''}
        {from === algebraicNotation && <img src={knight} alt="knight" />}
      </Square>
    )
  });

  return columns;

};

export default ChessBoardColumn;
