import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const AppRouter = (route) => {
  return (
    <Route path={route.path}
      render={props => (
        route.restrict && typeof route.restrict === 'function' && route.restrict() ? <Redirect to="/" />
          :
          <route.component {...props} routes={route.routes} />
      )} />
  );
};

export default AppRouter;
